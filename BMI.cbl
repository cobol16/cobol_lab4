       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. NATRUJA.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  HIEGHT      PIC 999V99.
       01  WEIGHT      PIC 999V99.
       01  BMI         PIC 999V99.
           88 UNDERWEIGHT    VALUE 0     THRU  18.4.
           88 NORMAL         VALUE 18.5  THRU  24.9.
           88 OVERWEIGHT     VALUE 25.0  THRU  39.9.
           88 OBESE          VALUE 40    THRU  1000.
       01  SHAPE        PIC X(12)  VALUE SPACE .
           88 SHAPE1         VALUE  "UNDERWEIGHT".
           88 SHAPE2         VALUE  "NORMAL".
           88 SHAPE3         VALUE  "OVERWEIGHT".
           88 SHAPE4         VALUE  "OBESE".

       PROCEDURE DIVISION. 
       BEGIN.
           DISPLAY "Input Weight(KG) - " WITH NO ADVANCING 
           ACCEPT WEIGHT 
           DISPLAY "Input Height(CM) - " WITH NO ADVANCING 
           ACCEPT HIEGHT 
           COMPUTE  BMI = WEIGHT /((HIEGHT / 100) * (HIEGHT / 100))
           DISPLAY "BMI  is " BMI 
           EVALUATE  TRUE 
              WHEN UNDERWEIGHT  SET SHAPE1 TO TRUE 
              WHEN NORMAL       SET SHAPE2 TO TRUE 
              WHEN OVERWEIGHT   SET SHAPE3 TO TRUE
              WHEN OBESE        SET SHAPE4 TO TRUE
           END-EVALUATE
           DISPLAY SHAPE 
           GOBACK.