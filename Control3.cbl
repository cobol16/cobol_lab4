       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL3.
       AUTHOR. NATRUJA.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  City-CODE PIC 9 VALUE ZERO .
           88 City-IS-DOUBLE       VALUE 1.
           88 City-IS-LIMERICK     VALUE 2.
           88 City-IS-CORK         VALUE 3.
           88 City-IS-GALWAY       VALUE 4.
           88 CITY-IS-SLIGO        VALUE 5.
           88 CITY-IS-WATERFORD    VALUE 6.
           88 UNIVERSITYCITY       VALUE 1 THRU 4.
           88 CITY-CODE-NOTVALID   VALUE 0, 7, 8, 9.
       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY  "Enter a city code (1-6) -" WITH NO ADVANCING 
           ACCEPT City-CODE 
           IF CITY-CODE-NOTVALID THEN
              DISPLAY "Invalid city code entered"
           ELSE 
              IF City-IS-LIMERICK THEN
                 DISPLAY "Hey, we're home."
              END-IF 
              IF City-IS-DOUBLE THEN
                 DISPLAY "Hey, we're in the capital."
              END-IF 
              IF UNIVERSITYCITY THEN
                 DISPLAY "Apply the rent surcharge!"
              END-IF 
           END-IF 
           SET  CITY-CODE-NOTVALID  TO  TRUE 
              DISPLAY City-CODE 
           GOBACK 
           .