       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL2.
       AUTHOR. NATRUJA.

       ENVIRONMENT DIVISION. 
       CONFIGURATION SECTION. 
       SPECIAL-NAMES. 
           CLASS  HEX-NUMBER IS "0" THRU "9", "A" THRU  "F"
           CLASS  REAL-NAME  IS "A" THRU "Z", "a" THRU  "z", "'", SPACE.
       
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  NUM-IN   PIC  X(4).
       01  NAME-IN  PIC  X(15).

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter a HEX number -" WITH NO ADVANCING 
           ACCEPT NUM-IN 
           IF NUM-IN IS HEX-NUMBER THEN 
              DISPLAY  NUM-IN  " is a Hex number"
           ELSE 
              DISPLAY  NUM-IN  " is not a Hex number"
           END-IF

           DISPLAY  "---------------------------------"
           DISPLAY  "Enter a name - " WITH NO ADVANCING 
           ACCEPT  NAME-IN 
           IF NAME-IN  IS ALPHABETIC 
              DISPLAY NAME-IN  " is alphabetic"
           ELSE 
              DISPLAY NAME-IN  " is not alphabetic" 
           END-IF 

           IF NAME-IN IS REAL-NAME THEN
              DISPLAY NAME-IN  " is a real name"
           ELSE 
              DISPLAY  NAME-IN  " is nat a real name"
           END-IF 
           STOP RUN.
           
           